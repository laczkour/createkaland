<?php 
/* include */ 
include 'editorpage.php';
/* connect */
$servername = "localhost";
$username = "root";
$password = "";
$database = "createkaland";

$conn = new mysqli($servername, $username, $password, $database);

/* mentés */
if (isset($_GET['save'])) {
    //var_dump($_GET);
    $storyid = $_GET['storyid'];
    $pageid = $_GET['pageid'];
    $storytext = $_GET['storytext'];
    $option_text1 = $_GET['option_text1'];
    $option_text2 = $_GET['option_text2'];
    $option_text3 = $_GET['option_text3'];
    $option_text4 = $_GET['option_text4'];
    $option1 = $_GET['option1'] === "" ? null : $_GET['option1'];
    $option2 = $_GET['option2'] === "" ? null : $_GET['option2'];
    $option3 = $_GET['option3'] === "" ? null : $_GET['option3'];
    $option4 = $_GET['option4'] === "" ? null : $_GET['option4'];



    $stmt = $conn->prepare("SELECT * FROM `page` WHERE storyid = ? AND pageid = ?");
    $stmt->bind_param("ii", $storyid, $pageid);
    $stmt->execute();
    $stmt->store_result();
    $sql = "";
    if ($stmt->num_rows > 0) {
        $sql = "UPDATE `page` SET 
        storytext = ?,
        option_text1 = ?,
        option_text2 = ?,
        option_text3 = ?,
        option_text4 = ?,
        option1 = ?,
        option2 = ?,
        option3 = ?,
        option4 = ?        
        WHERE storyid = ? AND pageid = ?";
    } else {
        $sql = "INSERT INTO `page` (
            storytext,
            option_text1,
            option_text2,
            option_text3,
            option_text4,
            option1,
            option2,
            option3,
            option4,
            storyid,
            pageid
        )
        VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    }
    $stmt->close();

    $stmt = $conn->prepare($sql);
    $stmt->bind_param("sssssiiiiii", $storytext,
        $option_text1,
        $option_text2,
        $option_text3,
        $option_text4,
        $option1,
        $option2,
        $option3,
        $option4,
        $storyid,
        $pageid);
    $stmt->execute();
    $stmt->close();
}


/* parameters */
$storyid = filter_input(INPUT_GET, 'storyid');
$pageid = filter_input(INPUT_GET, 'pageid');

$page = [];
$page['storyid'] = $storyid;
$page['pageid'] = $pageid;

$storyid = $storyid == null ? 0 : $storyid;
$pageid = $pageid == null ? 0 : $pageid;

/* oldal lekérése */
$stmt = $conn->prepare("SELECT * FROM `page` WHERE storyid = ? AND pageid = ?");
$stmt->bind_param("ii", $storyid, $pageid);
$stmt->execute();
$result = $stmt->get_result();


if ($result->num_rows > 0) {
    //var_dump($result);
    $row = $result->fetch_assoc();
    $page['storytext'] = $row['storytext'];
    $page['option_text1'] = $row['option_text1'];
    $page['option_text2'] = $row['option_text2'];
    $page['option_text3'] = $row['option_text3'];
    $page['option_text4'] = $row['option_text4'];
    $page['option1'] = $row['option1'];
    $page['option2'] = $row['option2'];
    $page['option3'] = $row['option3'];
    $page['option4'] = $row['option4'];    
}
$stmt->close();

/* megjelenites */


editorpage($page);