<?php 

$servername = "localhost";
$username = "root";
$password = "";
$database = "createkaland";

// Create connection
$conn = new mysqli($servername, $username, $password, $database);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
echo "<br>Connected successfully";

$sql = "DROP TABLE `page`";

if ($conn->query($sql) === TRUE) {
    echo "<br>Table page dropped successfully";
} else {
    echo "<br>Error dropping table: " . $conn->error;
}


$sql = "CREATE TABLE `page` ( 
`id` BIGINT NOT NULL AUTO_INCREMENT ,
`storyid` INT NOT NULL ,
`pageid` INT NOT NULL ,
`storytext` TEXT NOT NULL ,
`option_text1` TEXT ,
`option_text2` TEXT  ,
`option_text3` TEXT  , 
`option_text4` TEXT  , 
`option1` INT ,
`option2` INT  , 
`option3` INT  ,
`option4` INT  ,
PRIMARY KEY (`id`),
INDEX `pageindex` (`storyid`, `pageid`));";

if ($conn->query($sql) === TRUE) {
    echo "<br>Table createkaland created successfully";
} else {
    echo "<br>Error creating table: " . $conn->error;
}
  
  $conn->close();


